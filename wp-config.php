<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'canaan' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'tz<p OpYCejTG6mX`2=`i_2u6k%2d3gG^[&(zD3hQz2qc1xfi&Wre;+#Ygw(wNxA' );
define( 'SECURE_AUTH_KEY',  'K$~9&3`o.KP,Gw3v>r^II//p6V[|yq>^gB.a~?&Jj9! %nPwJehYMSEv~{$c:j&i' );
define( 'LOGGED_IN_KEY',    '-rYNX7ItzFy/bpZc)g2la;<7k-;Vd]O#ju)3 ms4O)qr_R=R],ngF9//qPPzc> u' );
define( 'NONCE_KEY',        '1(vT$Ya%LN=*H#R;3W$>yDvmycvOL9$7$yrT_co`W86#yj2tOF(R%P>rL:0d&<ZZ' );
define( 'AUTH_SALT',        'od0/7S c$1E%9Fx9}k.$>BOP-n <|.AAKPkAsN&TM,RuRgBYFRd{xaICX4,!s~si' );
define( 'SECURE_AUTH_SALT', '%%_IRa0E^mWKo^j]QDb~VmHYZtYCcM>$)U:L]C A@Jc^SZUUml|1%j*/mEdOfy6}' );
define( 'LOGGED_IN_SALT',   '7QlH?=97cdF@E{gweO&<>7mG,z`KQ{6+J.vL+4l#3y~|2_KBD}mCj~Jo5bEOga5X' );
define( 'NONCE_SALT',       ')G/jz-*PU24#6^tKE]DlSS&[`YLi*k|{H+i#2a/N`gkCe+s]wc<pndX^`B_&l$o:' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', true );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
